import React, { Component } from 'react';
import SearchForm from './containers/SearchForm';


class App extends Component {
  render() {
    return (
      <div>
        <SearchForm />
      </div>
    );
  }
}

export default App;
