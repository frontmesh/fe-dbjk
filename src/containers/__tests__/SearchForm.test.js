import React from 'react';
import { act } from 'react-dom/test-utils';
import { mount, shallow } from 'enzyme';

import SearchForm from '../SearchForm';
import Search from '../../components/Search/Search';
import Suggestions from '../../components/Suggestions/Suggestions';

import { makeSearch } from '../../services/searchService';
import data from '../__data__/_search.get';

jest.mock('../../services/searchService');

makeSearch.mockImplementation(() => ({
  data
}));

const createMount = () => {
  const c = mount(
    <SearchForm />
  );
  const search = c.find(Search);
  return {
    c,
    search,
    input: search.find('input'),
  }
}


describe('Search Form component', () => {
it('should render without failing', () => {
    const comp =  shallow(<SearchForm />);
    expect(comp).toMatchSnapshot();
  })

it('should make a search query and display suggestions', () => {
    const { c, input } = createMount();
    const q = 'heren'

    input.simulate('change', {target: { value: q }});
    input.simulate('keyup', {target: { value: q }});
    
    expect(makeSearch).toHaveBeenCalledWith(q);

    c.setState({ results: data.suggestions });

    expect(c.find(Suggestions).props()).toEqual({ query: q, results: data.suggestions });
    });

});