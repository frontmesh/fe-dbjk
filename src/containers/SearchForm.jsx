import React, { PureComponent } from 'react'
import throttle from 'lodash.throttle';

import Search from '../components/Search/Search'
import Suggestions from '../components/Suggestions/Suggestions'

import { makeSearch } from '../services/searchService';

const isValidQuery = qLength => qLength > 2;

class SearchForm extends PureComponent {

  constructor(props){
    super(props);
    this.state = {
      query: '',
      results: [],
    }
  }

  clearResults() {
    this.setState({
      results: []
    })
  }
    
  handleSearchInputKeyUp = async () => {
    const { query } = this.state;
    this.clearResults();
    if (isValidQuery(query.length)) {
      try {
        const response = await makeSearch(query);
        const { suggestions } = response.data;
        this.setState({
          results: suggestions
        })
      } catch (err) {
        console.error("Something went wrong while fetching suggestions")
      }
    }
  };

  setQuery = (query) => {
    this.setState({
      query
    });
  }

  render () {
    const { query, results } = this.state
    return (<div>
       <form id="searchform">
         <Search query={query} setQuery={this.setQuery} keyUpCallback={throttle(this.handleSearchInputKeyUp)} />
         {isValidQuery(query.length) && <Suggestions query={query} results={results} />}
       </form>
     </div>);
  }
}


export default SearchForm;