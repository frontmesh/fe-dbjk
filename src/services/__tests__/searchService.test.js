import { makeSearch } from '../searchService';
import { api } from '../config';

import data from '../../containers/__data__/_search.get'

jest.mock('../config');

test('should perform search query', () => {
  api.get.mockResolvedValue(data);

  return makeSearch().then(resp => expect(resp).toEqual(data));
});