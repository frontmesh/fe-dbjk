import axios from 'axios';

const API_URL = 'http://localhost:5000';

export const api = axios.create({
  timeout: 10000,
  baseURL: API_URL,
})