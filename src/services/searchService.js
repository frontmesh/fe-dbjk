import { api } from './config';

export const makeSearch = (q) => {
  return api.get("/search", {
    params: {
      q
    }
  });
};
