import React from 'react';
import { shallow } from 'enzyme';

import Search from '../Search';

const createShallow = (props) => {

  const h = {
    keyUpCallback: jest.fn(),
    setQuery: jest.fn(),
  };

  const c = shallow(
    <Search {...props} {...h} />
  );

  return {
    c,
    h,
    input: c.find('input'),
    // enzyme has a trouble finding d('svg') since they are represanted as FutureRef, in that case div#clear was used for testing
    clear: c.find('#clear'),
  }
}


describe('Search input component', () => {
  it('should render', () => {
    const { c, input } = createShallow();
    expect(c.type()).toEqual('div');
    expect(input.prop('placeholder')).toEqual('Zoeken');
    expect(c).toMatchSnapshot();
  })

  it('should save a query', () => {
    const { input, h } = createShallow();
    input.simulate('change', { target: {value: 'heren'}});

    expect(h.setQuery).toHaveBeenCalledTimes(1);
    expect(h.setQuery).toHaveBeenCalledWith('heren');
  });
  it('should have a clear button', ()=> {
    const { input, h, clear } = createShallow({ query: 'heren'});

    expect(input.prop('value')).toEqual('heren');
    expect(clear.type()).toBe('div');

    clear.simulate('click');
    expect(h.setQuery).toHaveBeenCalledTimes(1);
    expect(h.setQuery).toHaveBeenCalledWith('');
  });
})