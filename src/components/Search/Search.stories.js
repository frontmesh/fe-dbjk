import React from 'react';
import { action } from '@storybook/addon-actions';

import Search from './Search';

export default {
  component: Search,
  title: 'Search',
  excludeStories: /.*Data$/,
};

const actionsData = {
  keyUpCallback: action('keyUpCallback'),
  setQuery: action('setQuery') 
}

export const Default = () => {
  return <Search query="" {...actionsData} />
}

export const Active = () => {
  return <Search query="her" {...actionsData} />
}