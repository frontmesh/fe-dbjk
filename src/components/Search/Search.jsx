import React from "react";
import PropTypes from 'prop-types';

import { ReactComponent as SearchIcon } from './search.svg';
import { ReactComponent as DeleteIcon } from './delete.svg';
import styles from "./search.module.css";

const Search = ({ keyUpCallback, query, setQuery }) => {

  const handleKeyUp = (e) => {
    const query = e.target.value;
    if (typeof keyUpCallback === 'function') {
      keyUpCallback(query);
    }
  }

  const handleOnChange = (e) => {
    if (typeof setQuery === 'function') {
      setQuery(e.target.value)
    }
  }
  return (
    <div className={styles.container} tabIndex={-1}>
      <input
        type="text"
        className={styles.search}
        placeholder="Zoeken"
        onChange={handleOnChange}
        onKeyUp={handleKeyUp}
        value={query}
        tabIndex={1}
        autoComplete="off"
        autoFocus
        aria-live="polite"
        />
      {!!query &&
        <div id="clear" onClick={() => setQuery('')} >
          <DeleteIcon 
            className={styles.icon}
            fill="#343434"
            width="30px"
            height="30px"
          />
        </div>}
      <SearchIcon className={styles.icon} fill="#343434" width="30px" height="30px" />
    </div>
  );
}

Search.propTypes = {
  onQueryChange: PropTypes.func
}

export default Search;