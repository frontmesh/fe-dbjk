import React from 'react'
import { shallow } from 'enzyme';

import SuggestionItem from '../SuggestionItem';

const createShallow = (props) => {
  const c = shallow(
    <SuggestionItem {...props} />
  );

  return {
    c,
    li: c.find('li'),
    span: c.find('span')

  }
} 

describe('SuggestionItem component', () => {
  it('should render normaly without props', () => {
    const { c } = createShallow();
    expect(c).toMatchSnapshot();
  });

  it('should not fail rendering without a label', () => {
    const { c } = createShallow({ query: 'heren' });
    expect(c).toMatchSnapshot();
  });

  it('should render just with a label', () => {
    const { li } = createShallow({ label: 'heren truien' });

    expect(li.text()).toEqual('heren truien');
  });

  it('should hightlight query', () => {
    const { li, span } = createShallow({ label: 'heren truien', nr: 101, query: 'her' });

    expect(li.text()).toEqual('heren truien (101)');
    expect(span.at(0).text()).toEqual('her');
  });
})  