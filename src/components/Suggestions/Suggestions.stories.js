import React from 'react';
import data from '../../containers/__data__/_search.get'

import Suggestions from './Suggestions';

export default {
  component: Suggestions,
  title: 'Suggestions',
  excludeStories: /.*Data$/,
};

const suggestionsData = {
  results: data.suggestions,
  query: "her"
}

export const Default = () => {
  return <Suggestions { ...suggestionsData} query="" />
}


export const WithQuery = () => {
  return <Suggestions {...suggestionsData} />
}
