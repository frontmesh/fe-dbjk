import React from 'react'
import PropTypes from 'prop-types'

import SuggestionItem from './SuggestionItem';
import styles from "./suggestions.module.css";

const Suggestions = ({results, query}) => {
  if (!results) return null;

  return (
    <ul className={styles.suggestions}>
      {results.map((r) => 
        <SuggestionItem key={r.searchterm} label={r.searchterm} query={query} nr={r.nrResults}/>
      )}
    </ul>
  )
}

Suggestions.propTypes = {
  results: PropTypes.arrayOf(PropTypes.object),
  query: PropTypes.string
}

export default Suggestions;
