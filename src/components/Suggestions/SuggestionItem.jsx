import React from 'react'
import PropTypes from 'prop-types'

import styles from "./suggestionItem.module.css";

const SuggestionItem = ({query, label, nr}) => {
  
  if (!label) return null;
  
  const renderNum = nr && (<span> ({nr})</span>);

  if (!query) {
    return (<li className={styles.item} aria-label={label}>{label}{renderNum}</li>);
  }

  const re = new RegExp(query,"ig");

  return (
    <li className={styles.item} aria-label={label}>
      {label.split(re).reduce((acc, next, i) => {
          if (!i) {
            return [next];
          }
          const q = query.toLowerCase();
          return acc.concat(<span key={q + next}>{q}</span>, next);  
        },[])
      }
      {renderNum}
    </li>
  );
}

SuggestionItem.propTypes = {
  query: PropTypes.string,
  label: PropTypes.string,
  nr: PropTypes.number,
}

export default SuggestionItem;

